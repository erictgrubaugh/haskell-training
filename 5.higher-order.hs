--
-- Higher order functions
--

-- Haskell functions can take functions as parameters and return functions
-- as results (similar to JS where functions are first-class objects). These
-- are called higher order functions.

-- Curried functions
-- Functions in Haskell actually only take a single parameter. Any function
-- that accepts multiple parameters is actually a curried function. This is
-- why in type declarations that there is no distinction between input and
-- return types: because every function actually has a single input and output.
-- max :: (Ord a) => a -> a -> a can be rewritten as:
-- max :: (Ord a) => a -> (a -> a), meaning that max takes an a and returns
-- a function that takes an a and returns an a

-- Calling a function with too few parameters means we get back a
-- partially applied function. Thus we can do things like:
maxWithFour = max 4
seven = maxWithFour 7 -- 7
four = maxWithFour 3 -- 4

-- Infix functions can be partially applied by using sections:
divideByTen :: (Floating a) => a -> a
divideByTen = (/10)
twenty = divideByTen 200 -- 20

-- Because of sectioning, we can't use the subtraction operator in sections
-- Something like (-4) just means negative 4; in order to subtract in a section,
-- use the subtract function (subtract 4)

-- Example of higher order function
-- The parentheses around the first a -> a are now necessary because the ->
-- is right-associative
applyTwice :: (a -> a) -> a -> a
applyTwice f x = f (f x)

-- zipWith
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _ = []
zipWith' _ _ [] = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys

-- Since operators are just functions, we can do things like
addLists = zipWith' (+) [4,2,5,6] [2,5,2,3] -- [6,7,7,9]

-- flip - takes a function and returns a function with the arguments flipped
flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f y x = f x y

-- map - takes a function and a list and executes the function on each list
-- element
-- Typically what map can do could also be accomplished with a list
-- comprehension, but map is much more readable, particularly when
-- dealing with nested lists or maps of maps
map' :: (a -> b) -> [a] -> [b]
map' _ [] = []
map' f (x:xs) = f x : map' f xs

-- filter - accepts a predicate and a list and then only returns the list
-- of elements that satisfy the predicate
filter' :: (a -> Bool) -> [a] -> [a]
filter' _ [] = []
filter' p (x:xs)
	| p x = x : filter' p xs
	| otherwise = filter' p xs

-- There's no set rule for using map/filter versus using list comprehensions
-- Use context to decide which is more readable

-- Quicksort using filter
quicksortFilter :: (Ord a) => [a] -> [a]
quicksortFilter [] = []
quicksortFilter (x:xs) =
	let
		smallerSorted = quicksortFilter (filter (< x) xs)
		largerSorted = quicksortFilter (filter (>= x) xs)
	in
		smallerSorted ++ [x] ++ largerSorted
	
-- Find the largest number under 100,000 divisible by 3829
largestDivisible :: (Integral a) => a
largestDivisible = head (filter p [100000, 99999..])
	where
		p x = x `mod` 3829 == 0
-- ** I experimented quite a lot trying to simply write
-- ** code that would perform this calculation in GHCI
-- ** (i.e. not inside a function); it worked for smaller
-- ** datasets but would always crash when going up to
-- ** 100k
-- First iteration
-- ** I realize the quicksort is irrelevant because the
-- ** infinite list is already "sorted", but I wanted the
-- ** code to work for any arbitrary list
-- (crashes, takes far too long)
-- Based on this answer:
-- http://stackoverflow.com/questions/13541034/how-to-filter-an-infinite-list-in-haskell
-- this is because the list comprehension is evaluating every item in the
-- infinite list, rather than lazily stopping at the first match.
-- takeWhile won't apply here since my list isn't already sorted multiples
-- of 3829
largestDiv = head ([x | x <- reverse (take 100000 (quicksortFilter [1..])),
	x > 3829, x `mod` 3829 == 0])
-- Second iteration (still crashes)
-- I should be able to take advantage of the fact that there must be a multiple
-- of 3829 every 3829 elements, so I can at least reduce my data set down to
-- the first 3829 of the reversed, sorted list, but this still runs out of
-- memory. The problem lies in trying to sort an infinite list.
largestDiv' = head (
	[x | x <- take 3829 (reverse (take 100000 (quicksortFilter [1..]))),
		x `mod` 3829 == 0])
		
-- With my extremely basic knowledge of haskell at this point, I'm not sure
-- what tools I have at my disposal to work around this. Since the compiled
-- function using a filter works just fine, I'm sort of assuming this is
-- more a limitation of using ghci for this computation. I should be able
-- to at least re-write the original function to accept an arbitrary mod
-- and list
-- ** This assumes that the data coming in is sorted in descending order already
-- I may revisit when I understand more about sorting and haskell in general
largestDivisible' :: (Integral a) => a -> [a] -> a
largestDivisible' m xs = head (filter p xs)
	where
		p x = x `mod` m == 0
		
-- Find the sum of all odd squares that are smaller than 10,000
sumOddSquares = sum (takeWhile (<10000) (filter odd (map (^ 2) [1..])))

-- Collatz sequences
-- Take any natural number: if it is even, divide by 2; if it is odd, multiply
-- by 3 and add 1; repeat. Hypothesis is that for all starting numbers, the
-- sequence will end with 1.
collatz :: (Integral a) => a -> [a]
collatz 1 = [1]
collatz n
	| n <= 0 = []
	| odd n = n:collatz (n * 3 + 1)
	| even n = n:collatz (n `div` 2)
	
-- For all starting numbers from 1 to 100, how many Collatz sequences have
-- a length of greater than 15?
countLongChains :: Int
countLongChains = length (filter (> 15) (map (length) (map (collatz) [1..100])))
-- The book's solution is more condensed, combining my length mapping and
-- filtering into a single filter function
countLongChains' :: Int
countLongChains' = length (filter isLong (map collatz [1..100]))
	where
		isLong xs = length xs > 15
-- Not exactly sure how to compare these in terms of performance, though it
-- seems that the second solution's combination of my map and filter steps
-- would do less processing.

-- Currying note
-- map can also be used to generate a list of partially applied functions
-- To illustrate, consider
multiplyByFunctions = map (*) [0..]
-- This creates a list of partially applied multiplication functions.
-- Since * takes two parameters, but we've only passed it one through
-- the call to map, our resulting list is a set of functions that accept
-- one parameter, something like [(0*),(1*),(2*),..]
sevenTimesFive = (multiplyByFunctions !! 7) 5 -- 35

-- Lambdas
-- Anonymous functions that are only needed once, usually only used to
-- pass to a higher-order function. Syntax is:
-- (\<parameters> -> <function body>)
-- For instance, rewriting countLongChains with a lambda looks like:
countLongChains'' :: Int
countLongChains'' = length (filter (\xs -> length xs > 15) (map collatz [1..100]))
-- The lambda replaces the isLong declaration
-- Author references a common mistake as using lambdas where partial application
-- would suffice, citing readability

-- Lambdas can pattern match, but you cannot define several patterns for one
-- parameter; when pattern matching fails in lambdas, a runtime error occurs

-- Lambdas will extend to the right unless wrapped in parens
-- The following are equivalent:
addThree :: (Num a) => a -> a -> a -> a
addThree x y z = x + y + z

addThree' :: (Num a) => a -> a -> a -> a
addThree' \x -> \y -> \z -> x + y + z