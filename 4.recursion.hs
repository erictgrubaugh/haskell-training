--
-- Recursion
--

-- Recursion occurs when a function is applied inside its own definition
-- Recursion requires an edge condition (I learned this as "terminating
-- condition") in order to complete; otherwise the recursion would be
-- infinte.
-- Haskell performs computations by declaring what something is, not
-- how to get it (like imperative style). With no loop structures,
-- we often fall back on recursion

-- Defining maximum recursively
-- Split the list into a head and tail and compare the head to the
-- maximum of the tail
-- * At this point I am still thinking very imperatively. My initial
-- * thought was that you could just sort the list and grab the
-- * first (or last) element, but that is defining *how* to get the
-- * maximum, not *what* the maximum is. Instead, we just say "the
-- * maximum of a list is the larger of its head and the maximum
-- * of its tail"
maximum' :: (Ord a) => [a] -> a
maximum' [] = error "Empty lists don't have maxima!"
maximum' [x] = x
maximum' (x:xs) = max x (maximum' xs)

-- Replicate recursively
-- Use guards instead of patterns as we're checking Boolean conditions
-- Num is not a subclass of Ord, so when we are adding/subtracing and also
-- comparing, we need both typeclasses
replicate' :: (Num i, Ord i) => i -> a -> [a]
replicate' n x
	| n <= 0 = []
	| otherwise = x:replicate' (n-1) x
	
-- Take recursively
-- There's no otherwise on the first guard so that fall-through continues
-- If n is less than or equal to 0, we don't care what we're taking from,
-- we'll get an empty list
-- Similarly, no matter what we're taking from an empty list, we get an
-- empty list
-- Our recursive case is the typical split into head and tail
take' :: (Num i, Ord i) => i -> [a] -> [a]
take' n _
	| n <= 0 = []
take' _ [] = []
take' n (x:xs) = x:take' (n-1) xs

-- Reverse recursively
-- Have to use ++ because can't use : with a list on the left
reverse' :: [a] -> [a]
reverse' [] = []
reverse' (x:xs) = reverse' xs ++ [x]

-- Repeat recursively
-- With no terminating condition, this makes an infinite list (which is OK!)
repeat' :: a -> [a]
repeat' n = n:repeat' n

-- Zip recursively
-- zip takes two lists and zips them together into pairs
zip' :: [a] -> [b] -> [(a,b)]
zip' _ [] = []
zip' [] _ = []
zip' (x:xs) (y:ys) = (x, y):zip' xs ys

-- Elem recursively
-- elem takes an element and a list and sees if that element is in the list
-- Inputs must be in typeclass Eq since we're testing equality
elem' :: (Eq a) => a -> [a] -> Bool
_ `elem'` [] = False
n `elem'` (x:xs)
	| n == x = True
	| otherwise = n `elem'` xs
	
-- Quicksort
quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = quicksort smaller ++ [x] ++ quicksort larger
	where
		smaller = [y | y <- xs, y <= x]
		larger = [y | y <- xs, y > x]