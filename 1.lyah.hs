--
-- Starting Out
-- Notes from "Starting Out" from Learn You a Haskell
-- learnyouahaskell.com
--

-- Haskell is purely functional
-- Haskell performs lazy evaluation

-- Mathematical and logical operators are consistent
-- with most other languages
-- Modulus is performed with the `mod` infix function
doubleMe :: (Num a) => a -> a
doubleMe x = x + x

doubleAndSum :: (Num a) => a -> a -> a
doubleAndSum x y = doubleMe x + doubleMe y

-- Logic

-- Functions can be called either infix or prefix
-- Infix requires backticks
x `and` y = x && y

x `or` y = x || y

x `xor` y = (not x && y) || (x && not y)

-- Comparison is done with == and /=
isEqual :: (Eq a) => a -> a -> Bool
x `isEqual` y = x == y

isNotEqual :: (Eq a) => a -> a -> Bool
x `isNotEqual` y = x /= y

-- The ' has no special meaning itself, but it is often used to denote
-- slightly modified versions of similar functions
isNotEqual' :: (Eq a) => a -> a -> Bool
x `isNotEqual'` y = not (x `isEqual` y)

-- else clauses are mandatory in haskell since
-- all expressions must return a value
doubleSmallNumber x = if x > 100
	then x
	else x * 2

doubleSmallNumber' x = (if x > 100
	then x
	else x * 2) + 1
	
-- Lists

-- List concatenation uses the ++ operator
-- This should be used with care as Haskell must walk the entire left-hand
-- list before appending
-- The ++ operator requires two lists
concatenate :: [a] -> [a] -> [a]
x `concatenate` y = x ++ y

-- Elements can also be "shifted" on to the front of a list instantaneously
-- with the cons (:) operator
-- The cons operator requires a single element and one list
shift :: a -> [a] -> [a]
shift x xs = x:xs

-- Elements can be retrieved from lists with !!
--at :: (Int b) => [a] -> b -> a
x `at` y = x !! y

-- Lists can be compared if their elements can be compared
-- Comparison is performed lexicographically in forward order

-- Basic List functions:
-- head - takes a list and returns first element
-- tail - takes a list and returns all but first element
-- last - takes a list and returns last element
-- init - takes a list and returns all but last element
-- ** Calling the previous on an empty list will throw error
-- length - takes a list and returns the number of elements
-- null - checks if a list is empty; use instead of xs == []
-- reverse - takes a list and returns its elements in reverse
-- take - takes a number and a list and extracts that many elements from the
-- 		beginning of the list
-- drop - takes a number and a list and removes that many elements from the
--		beginning of the list
-- maximum - takes a list of Order-able elements and returns the largest
-- minimum - takes a list of Order-able elements and returns the smallest
-- sum - takes a Numeric list and returns the sum of all elements
-- product - takes a Numeric list and returns the product of all elements
-- elem - takes a thing and a list and returns True if the thing is in the list;
-- 		returns False otherwise

-- Ranges

-- Ranges can be used to quickly generate a sequence of enumerable elements:
oneToTwenty = [1..20] -- generates all integers from 1 to 20
-- A step can be provided (but only a *single* step):
oneToTenEvens = [0,2..10] -- generates all even numbers from 0 to 10
-- A step *must* be specified when going backwards:
twentyToOne = [20,19..1] -- generates all integers from 20 down to 1
-- Infinite lists can be created by not specifying an upper limit
-- Lazy evaluation means Haskell will only compute what you request
-- from the list
infiniteMultiples = [13,26..] -- generates all multiples of 13

-- Some infinite List functions:
-- cycle - takes a list and repeats the elements infinitely
-- repeat - takes an element and repeats it infinitely
-- replicate - takes an element and a number and repeats the element that number
--		of times (not an infinite list, but similar to repeat with a limit)

-- List Comprehensions

-- List comprehensions allow us to generate lists based on an input list
-- potentially filtered by predicates and then piped through an output function
-- Similar to Set Comprehensions in mathematics and Array.map in JS

-- "x is drawn from [0..10], then x is doubled"
tenDoubles = [x * 2 | x <- [0..10]]

-- "x is drawn from [0..10] where x * 2 is greater than 10, then x is doubled"
tenDoubles' = [x * 2 | x <- [0..10], x * 2 > 10]

-- "x is drawn from [50..100] where the remainder of x divided by 7 is 3"
modSevenIsThree = [x | x <- [50..100], x `mod` 7 == 3]

-- We can draw from multiple lists
-- The comprehension will produce results for all combinations of the list
--		elements
products = [ x * y | x <- [2,3,5], y <- [4,8,7]]

-- Use _ to indicate that we don't care what value goes here
length' xs = sum [1 | _ <- xs]

-- Tuples

-- Tuples do not have to be homogeneous
-- Lists of Tuples must have the same amount and types of elements
-- There are no single Tuples because this doesn't make sense

-- Some Tuple functions:
-- fst - takes a pair and returns its first element
-- snd - takes a pair and returns its second element
-- zip - takes two lists and joins the matching elements into pairs; if the
--		lengths do not match, the longer list is simply truncated