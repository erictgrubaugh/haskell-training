--
-- Types and Typeclasses
--

-- Haskell is statically typed, but it also has type inference capabilities

-- Use the :t command in ghci to determine the type of any valid expression

-- The :: operator indicates type, and is read "has type of"

-- Strings are actually a list of Chars

-- Each tuple is its own type

-- Functions can have explicit type declarations
-- "removeNonUppercase maps from a string to a string"
removeNonUppercase :: [Char] -> [Char]
removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z'] ]

-- There is no special distinction between parameters and return type
-- The return type is simply the last one listed
addThree :: Int -> Int -> Int -> Int
addThree x y z = x + y + z

-- Common Types

-- Int - Bounded integers; range depends on system architecture (32 v 64)
-- Integer - Unbounded integers; less efficient than Int counterpart
-- Float - single-precision floating point
-- Double - double-precision floating point
-- Bool - Boolean values
-- Char - character; denoted by single quotes; a list of Chars is a String

-- Type Variables

-- Type variables in Haskell allow us to genericize functions,
-- making them polymorphic
-- Typically use a, b, c, d for type variable names

-- Typeclasses

-- Similar to interfaces in other languages (*not* Classes)
-- => denotes class constraints

-- Eq - types that support equality testing using == and /=
-- Ord - types that have an ordering;
-- 		covers comparing functions > < >= <=;
--		a type must first be Eq before it can be Ord
-- Show - types that can be presented as Strings
-- Read - types that can be read from Strings
-- Enum - sequentially ordered types; can be used in Ranges;
--		have defined successors and predecessors
-- Bounded - types that have an upper and a lower bound
-- Num - numeric types; type must first be in Show and Eq
-- Integral - only whole numbers (Int, Integer)
-- Floating - only floating point numbers (Float, Double)

-- Very useful class diagram: http://en.wikibooks.org/wiki/File%3aClasses.png

-- Type Annotations

-- Used to explicitly say what type an expression should be
-- Use the :: operator at the end of an expression
read "5" :: Int