--
-- Syntax in Functions
--

-- Pattern Matching

-- Allows us to define separate function bodies for different patterns
-- Patterns are checked from top to bottom
-- Cleaner syntax than a complex if-else structure
-- Should always include a catch-all pattern to avoid runtime exceptions
-- Similar to default case in switch statement
sayMe :: (Integral a) => a -> String
sayMe 1 = "One"
sayMe 2 = "Two"
sayMe 3 = "Three"
sayMe 4 = "Four"
sayMe 5 = "Five"
sayMe x = "Other"

-- Recursion ftw
factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n - 1)

-- Adding vectors without pattern matching
addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors a b = (fst a + fst b, snd a + snd b)

-- Pattern matching counterpart
addVectors' :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors' (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

-- Implementing "third", like fst and snd
third :: (a, b, c) -> c
third (_, _, z) = z

-- Common pattern for pattern matching on lists is to use
-- x:xs - this binds the head of a list to x and the rest to xs
-- It will match any list of length 1 or more
-- We can extend this pattern to something like
-- x:y:z:zs - this will bind the first three elements to variables
-- and will only match lists of length 3 or more

-- Implementing head using pattern matching
-- Any time we need to bind multiple variables, even if one is _,
-- they must be enclosed in ()
head' :: [a] -> a
head' [] = error "An empty list is already decapitated."
head' (x:_) = x

-- Yet another length implementation with pattern matching
length'' :: (Num b) => [a] -> b
length'' [] = 0
length'' (_:xs) = 1 + length'' xs

-- Sum implementation with pattern matching
sum' :: (Num a) => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs

-- As Patterns
-- Allow us to bind pattern matches to a name for easier reference
-- syntax: name@pattern
-- We cannot use ++ directly in pattern matches
capital :: String -> String
capital "" = "No capitals in the empty string!1!"
capital all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]

-- Guards

-- Patterns help us match on forms of data; guards help us check conditions
-- Similar to if statements, but more readable and play nicely with patterns
-- Author assures me that guards are "far better" than ifs, but doesn't explain
-- Like patterns, guards are evaluated top to bottom
bmiTell :: (RealFloat a) => a -> String
bmiTell bmi
	| bmi <= 18.5 = "Underweight"
	| bmi <= 25.0 = "Normal"
	| bmi <= 30.0 = "Overweight"
	| otherwise = "Obese"
	
-- Guards can be used on functions with multiple parameters
bmiTell' :: (RealFloat a) => a -> a -> String
bmiTell' weight height
	| weight / height ^ 2 <= 18.5 = "Underweight"
	| weight / height ^ 2 <= 25.0 = "Normal"
	| weight / height ^ 2 <= 30.0 = "Overweight"
	| otherwise = "Obese"
	
-- Where clause
-- Lets us bind an expression to a name and reference, cutting down repetition
-- Can bind multiple expressions
-- Names defined in where clause are only visible to the function, nowhere else
-- Names must be aligned or Haskell won't know they're in the same block
-- ** This indentation was a pain in the ass at first; I kept getting compile
-- ** errors when trying to keep the where and the first expression on the
-- ** same line. Moving the first expression to the next line and indenting
-- ** fixed that, and is also a much more readable syntax for me.
-- Where bindings are not shared across different pattern matches
-- Where clauses can be nested; it is common practice to define a function,
-- give it helper functions inside of its where clause, and give the helpers
-- their own helpers in their own where clauses (where-ception)
bmiTell'' :: (RealFloat a) => a -> a -> String
bmiTell'' weight height
	| bmi <= underweight = "Underweight"
	| bmi <= normal = "Normal"
	| bmi <= overweight = "Overweight"
	| otherwise = "Obese"
	where
		bmi = weight / height ^ 2
		(underweight, normal, overweight) = (18.5, 25.0, 30.0)
	
-- Implementing max with guards
max' :: (Ord a) => a -> a -> a
max' x y
	| x < y = y
	| otherwise = x
	
-- Implementing compare with guards
compare' :: (Ord a) => a -> a -> Ordering
x `compare'` y
	| x > y = GT
	| x < y = LT
	| otherwise = EQ
	
-- Functions can also be defined in where blocks
-- This function takes a list of weight-height pairs and calculates all BMIs
-- ** This is such little code to accomplish this task!
calcBmis :: (RealFloat a) => [(a, a)] -> [a]
calcBmis xs = [ bmi w h | (w, h) <- xs ]
	where
		bmi weight height = weight / height ^ 2
		
-- let bindings

-- While where bindings let us bind variables that the entire function can see,
-- let bindings allow us to bind variables in very local scopes
-- let bindings do not span across guards
-- syntax: let <bindings> in <expression>
-- The bindings in the let portion are only accessible to the in expression
cylinderArea :: (RealFloat a) => a -> a -> a
cylinderArea r h =
	let
		sideArea = 2 * pi * r * h
		topArea = pi * r ^ 2
	in
		sideArea + 2 * topArea
		
-- let bindings are simply expressions, so they can be placed almost anywhere
inlineLet = 4 * (let a = 9 in a + 1) + 2 -- 42

-- let bindings can introduce functions in local scope
squares = [let square x = x * x in (square 5, square 3, square 2)] -- [(25,9,4)]

-- When binding several variables inline, we can separate with semicolons
multiVars = (let a = 100; b = 200; c = 300; in a * b * c) -- 6000000

-- Pattern matching can be used in let bindings; this is very useful for
-- dismantling tuples and binding to names
dismantle = (let (a,b,c) = (1,2,3) in a+b+c) * 100 -- 600

-- let bindings can be used inside list comprehensions
-- They look like predicates, but do not actually filter the list
-- These bindings are visible to the output function as well as any predicates
-- or other sections after the let
calcBmis' :: (RealFloat a) => [(a, a)] -> [a]
calcBmis' xs = [bmi | (w, h) <- xs, let bmi = w / h ^ 2]

-- Case expressions

-- Pattern matching parameters in function definitions is really just
-- a special instance of a Case expression, syntactic sugar

-- Compare head' above with its Case expression counterpart here
-- The two versions are exactly the same, interchangeable
head'' :: [a] -> a
head'' xs = case xs
			of
				[] -> error "An empty list is already decapitated."
				(x:_) -> x

-- As usual, the first pattern matched is what gets used
-- A runtime error occurs if no suitable pattern is found