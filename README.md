# README #

### What is this repository for? ###

This repository simply contains my notes and tests from learning Haskell,
mostly through [Learn You a Haskell](http://learnyouahaskell.com/)

*References*

* [Learn You a Haskell](http://learnyouahaskell.com/)
* [Real World Haskell](http://book.realworldhaskell.org/)
* [A Gentle Introduction to Haskell](https://www.haskell.org/tutorial/)
* [An Introduction to Computing with Haskell](https://www.cse.unsw.edu.au/~chak/papers/intro-computing.pdf)
